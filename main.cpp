#include <iostream>
#include <vector>
#include <cstdlib>
#include <string.h>
#include <algorithm>

void test();
void printAllNumbers(int argc, const char* argv[]);

int main(int argc, const char* argv[])
{
#ifdef SUT
    test();
#else
    printAllNumbers(argc, argv);
#endif
}

std::vector<int> parseAndSort(int argc, const char* argv[]);
std::string convertToRoman(int num);

void printAllNumbers(int argc, const char* argv[])
{
    std::vector<int> values = parseAndSort(argc, argv);
    for (int i = 0; i < values.size(); i++)
    {
        std::cout << convertToRoman(values[i]) << std::endl;
    }

}

bool parsingSuccessful(const char* parseStart, const char* parseEnd)
{
    return parseEnd - parseStart == strlen(parseStart);
}

bool numberInRange(int num)
{
    return num > 0 && num <= 5000;
}

std::vector<int> parseAndSort(int argc, const char* argv[])
{
    std::vector<int> retval;

    char * p = NULL;

    for (int i = 1; i < argc; i++)
    {
        const char * str = argv[i];

        int iNum = strtol(str, &p, 0);

        if (parsingSuccessful(str, p) && numberInRange(iNum))
        {
            retval.push_back(iNum);
        }
    }

    std::sort(retval.begin(), retval.end());
    return retval;
}

typedef std::pair<int, std::string> RomanPair;
typedef std::vector<RomanPair> RomanPairs;

RomanPairs& getRomanPairs()
{
    static RomanPairs romanPairs;

    if (romanPairs.empty())
    {
        romanPairs.push_back(RomanPair(5000, "|L|"));
        romanPairs.push_back(RomanPair(4000, "M|L|"));
        romanPairs.push_back(RomanPair(1000, "M"));
        romanPairs.push_back(RomanPair(900, "CM"));
        romanPairs.push_back(RomanPair(500, "D"));
        romanPairs.push_back(RomanPair(400, "CD"));
        romanPairs.push_back(RomanPair(100, "C"));
        romanPairs.push_back(RomanPair(90, "XC"));
        romanPairs.push_back(RomanPair(50, "L"));
        romanPairs.push_back(RomanPair(40, "XL"));
        romanPairs.push_back(RomanPair(10, "X"));
        romanPairs.push_back(RomanPair(9, "IX"));
        romanPairs.push_back(RomanPair(5, "V"));
        romanPairs.push_back(RomanPair(4, "IV"));
        romanPairs.push_back(RomanPair(1, "I"));
    }

    return romanPairs;
}

std::string convertToRoman(int num)
{
    std::string rom;

    RomanPairs &romanPairs = getRomanPairs();

    for(int i = 0; i < romanPairs.size(); i++)
    {
        int dec = romanPairs[i].first;
        std::string str = romanPairs[i].second;

        while(num >= dec)
        {
            rom += str;
            num -= dec;
        }

    }

    return rom;
}

#ifdef SUT
#include "tests.cpp"
#endif
