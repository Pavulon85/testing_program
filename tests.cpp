using namespace std;

#define CHECK(expected, actual) expected == actual ? cout << "OK\n" : cout << "NOK: expected: " << expected << ", actual: " << actual << "\n"

void test()
{
    const char *argv[] = {"cos", "124"};
    int argc = sizeof(argv)/sizeof(argv[0]);

    std::vector<int> values = parseAndSort(argc, argv);


    CHECK(1, values.size());
    CHECK(124, values[0]);

    //===========================================
    const char *argv1[] = {"cos"};
    argc = sizeof(argv1)/sizeof(argv1[0]);

    std::vector<int> values1 = parseAndSort(argc, argv1);

    CHECK(0, values1.size());

   //=============================================
    const char *argv2[] = {"cos", "666", "0xfff"};
    argc = sizeof(argv2)/sizeof(argv2[0]);

    std::vector<int> values2 = parseAndSort(argc, argv2);


    CHECK(2, values2.size());
    CHECK(666, values2[0]);
    CHECK(0xfff, values2[1]);

    //=============================================
    const char *argv3[] = {"cos", "dupa", "0xfff"};
    argc = sizeof(argv3)/sizeof(argv3[0]);

    std::vector<int> values3 = parseAndSort(argc, argv3);


    CHECK(1, values3.size());
    CHECK(0xfff, values3[0]);

    //=============================================
    const char *argv8[] = {"cos", "-100", "0xfff"};
    argc = sizeof(argv8)/sizeof(argv8[0]);

    std::vector<int> values8 = parseAndSort(argc, argv8);


    CHECK(1, values8.size());
    CHECK(0xfff, values8[0]);

    //=============================================
    const char *argv9[] = {"cos", "5001", "5000"};
    argc = sizeof(argv9)/sizeof(argv9[0]);

    std::vector<int> values9 = parseAndSort(argc, argv9);

    CHECK(1, values9.size());
    CHECK(5000, values9[0]);

    //=============================================
    const char *argv4[] = {"cos", "dupa", "0x1123", "dupa2", "0242"};
    argc = sizeof(argv4)/sizeof(argv4[0]);

    std::vector<int> values4 = parseAndSort(argc, argv4);


    CHECK(2, values4.size());
    CHECK(0242, values4[0]);
    CHECK(0x1123, values4[1]);

    //=============================================

    std::string romanNum = convertToRoman(1);
    CHECK("I", romanNum);

    romanNum = convertToRoman(2);
    CHECK("II", romanNum);

    romanNum = convertToRoman(3);
    CHECK("III", romanNum);

    romanNum = convertToRoman(4);
    CHECK("IV", romanNum);

    romanNum = convertToRoman(5);
    CHECK("V", romanNum);

    romanNum = convertToRoman(6);
    CHECK("VI", romanNum);

    romanNum = convertToRoman(7);
    CHECK("VII", romanNum);

    romanNum = convertToRoman(8);
    CHECK("VIII", romanNum);

    romanNum = convertToRoman(9);
    CHECK("IX", romanNum);

    romanNum = convertToRoman(10);
    CHECK("X", romanNum);

    romanNum = convertToRoman(11);
    CHECK("XI", romanNum);

    romanNum = convertToRoman(12);
    CHECK("XII", romanNum);

    romanNum = convertToRoman(13);
    CHECK("XIII", romanNum);

    romanNum = convertToRoman(14);
    CHECK("XIV", romanNum);

    romanNum = convertToRoman(15);
    CHECK("XV", romanNum);

    romanNum = convertToRoman(16);
    CHECK("XVI", romanNum);

    romanNum = convertToRoman(19);
    CHECK("XIX", romanNum);

    romanNum = convertToRoman(20);
    CHECK("XX", romanNum);

    romanNum = convertToRoman(21);
    CHECK("XXI", romanNum);

    romanNum = convertToRoman(39);
    CHECK("XXXIX", romanNum);

    romanNum = convertToRoman(48);
    CHECK("XLVIII", romanNum);

    romanNum = convertToRoman(50);
    CHECK("L", romanNum);

    romanNum = convertToRoman(66);
    CHECK("LXVI", romanNum);

    romanNum = convertToRoman(90);
    CHECK("XC", romanNum);

    romanNum = convertToRoman(100);
    CHECK("C", romanNum);

    romanNum = convertToRoman(400);
    CHECK("CD", romanNum);

    romanNum = convertToRoman(500);
    CHECK("D", romanNum);

    romanNum = convertToRoman(499);
    CHECK("CDXCIX", romanNum);

    romanNum = convertToRoman(900);
    CHECK("CM", romanNum);

    romanNum = convertToRoman(1000);
    CHECK("M", romanNum);

    romanNum = convertToRoman(2000);
    CHECK("MM", romanNum);

    romanNum = convertToRoman(4000);
    CHECK("M|L|", romanNum);

    romanNum = convertToRoman(5000);
    CHECK("|L|", romanNum);


}

